<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
    }
    public function index()
    {
        // has_loggedin();

        set_rules("email", "Email", "required");
        set_rules("password", "Password", "required");
        
        if ($this->form_validation->run() == False) {
            $data['title'] = "Login";
            $this->load->view('templates/auth_header', $data);
            $this->load->view('auth/login');
            $this->load->view('templates/auth_footer');
        } else {
            $data = [
                "email" => post("email"),
                "password" => post("password")
            ];
            $dbget = dbgetwhere("users", ['email' => $data['email']])->row_array();
            $profile = dbgetwhere("role", ['id_role' => $dbget['id_role']])->row_array();
            // dd(crypt($data['password'], $dbget['password']));
            if ($dbget) {
                if (password_verify($data['password'], $dbget['password'])) {
                    set_userdata("username", $dbget['nama']);
                    set_userdata("user_id", $dbget['id_user']);
                    set_userdata("role", $profile['nama_role']);
                    if($dbget['id_role'] == 1){
                        redirect(base_url('dapur'));
                    }
                    else{
                        redirect(base_url('dashboard'));
                    }
                } else {
                    set_flashdata("msg", swalfire("Password Salah!","error"));
                    redirect(base_url('auth'));
                     
                }
            } else {
                set_flashdata("msg", swalfire("User Tidak Terdaftar!","error"));
                // dd(flashdata("msg"));
                redirect(base_url('auth'));
                
            }
        }
    }

    // public function diduka(){
    //     $data['title'] = "Login";
    //     $this->load->view('templates/auth_header', $data);
    //     $this->load->view('auth/login-diduka');
    //     $this->load->view('templates/auth_footer'); 
    // }

    // public function admin(){
    //     $data['title'] = "Login";
    //     $this->load->view('templates/auth_header', $data);
    //     $this->load->view('auth/admin/index');
    //     $this->load->view('templates/auth_footer'); 
    // }

    public function register(){
        // check_rule(false,"is_create",true);
		set_rules('nama', 'nama', 'required');
		set_rules('email', 'email', 'required|valid_email');
		// set_rules('profile_type', 'profile type', 'required');
        set_rules('password1', 'password', 'required|trim|min_length[3]|matches[password2]',['matches' => 'password dont match!', 'min_length' => 'password too short!']);
        set_rules('password2', 'password', 'required|trim|matches[password1]');
		
		if ($this->form_validation->run() == False) {
			$data["title"] = "Registrasi";
			// $data["profile_type"] = dbget('profile_type')->result_array();
			$this->load->view('templates/auth_header', $data);
            $this->load->view('auth/registration');
            $this->load->view('templates/auth_footer');
		} else {
			$data = [
				"nama" => post('nama'),
				"email" => post('email'),
				"password" => password_hash(post('password1'), PASSWORD_DEFAULT),
				"id_profile_type" => post('profile_type'),
				"is_active" => 1,
				"created_at" => date("Y-m-d H:i:s")
			];
			dbinsert('users',$data);
            // $last_id = dbinsertid();
            // $profile = post("profile_type");
            // if($profile == 1 ){
            //     $data = [
            //         "id_user" => $last_id,
            //         "nama" => post('nama'),
            //         "created_at" => date("Y-m-d H:i:s")
            //     ];
            // dbinsert('siswa',$data);
            // }
            // if($profile == 2 ){
            //     $data = [
            //         "id_user" => $last_id,
            //         "nama" => post('nama'),
            //         "email" => post('email'),
            //         "created_at" => date("Y-m-d H:i:s"),
            //         "created_by" => post('nama')
            //     ];
            // dbinsert('diduka',$data);
            // }
            // if($profile == 3 ){
            //     $data = [
            //         "id_user" => $last_id,
            //         "nama" => post('nama'),
            //         "created_at" => date("Y-m-d H:i:s")
            //     ];
            // dbinsert('guru',$data);
            // }
            // if($profile == 4 ){
            //     $data = [
            //         "id_user" => $last_id,
            //         "nama" => post('nama'),
            //         "created_at" => date("Y-m-d H:i:s")
            //     ];
            // dbinsert('pembimbing',$data);
            // }
			set_flashdata("msg", swalfire('Data Berhasil Dimasukan', 'success'));
			redirect(base_url("Auth/index"));
		}
    }
    
    // public function registration()
    // {
    //     $this->form_validation->set_rules('name', 'Name', 'required|trim');
    //     $this->form_validation->set_rules('email', 'email', 'required|trim|valid_email');
    //     $this->form_validation->set_rules('password1', 'password', 'required|trim|min_length[3]|matches[password2]',['matches' => 'password dont match!', 'min_length' => 'password too short!']);
    //     $this->form_validation->set_rules('password1', 'password', 'required|trim|matches[password1]');
        
    //     if ($this->form_validation->run() == false)
    //     {
    //         $data['title'] = 'Registration Page';
    //         $this->load->view('templates/auth_header', $data);
    //         $this->load->view('auth/registration');
    //         $this->load->view('templates/auth_footer');
    //     }
    //     else 
    //     {
    //        $data = [
    //            'nama' => $this->input->post('name'),
    //            'email' => $this->input->post('email'),
    //            'password' => password_hash($this->input->post('password'), PASSWORD_DEFAULT),
    //        ];

    //        $this->db->insert('users', $data);
    //        redirect('auth');
    //     }
    // }

    public function forbidden()
    {
        view("forbidden");
    }

    public function logout()
    {
        unset_userdata("username");
        unset_userdata("role");
        unset_userdata("user_id");
        set_flashdata("msg", swalfire("Logout Berhasil", "success"));
        redirect(base_url("auth"));
    }
}

