<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Event extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model("EventModel");
		$this->load->model("UserModel");
        $this->load->helper('file');
        $this->load->library('upload');
	}

	public function index()
	{
		has_loggedin();
		// dd(ormawadata('role_id'));
		// check_rule(false,"is_read",true);
		$data["title"] = "EVEJA";
		// $data["profile_style"] = rawview("templates/profile_style");
		$data["sidebar_style"] = rawview("templates/sidebar_style");
		$data["topbar_style"] = rawview("templates/topbar_style", $data);
		$data["content"] = rawview("event/index");
		// // // // // $data["footer_style"] = rawview("templates/footer_style");
		view('templates/dashboard_style', $data);
	}

	public function datatable()
	{
		// check_rule(false,"is_read",true);

		$show = [];
		
		// $order_column = post("order")[0]['column'] == 0;
		// $order_dir = post("order")[0]['dir'] == "asc";
		// $i = ($order_column && $order_dir) ? post("start") + 1 : $this->ormawaModel->get_filtered_data() - post("start");
		foreach ($this->EventModel->datatable() as $key) {
			$data = [];
			// $data[] = ($order_column && $order_dir) ? $i++ : $i--;
			$data[] = $key['nama_event'];
			$data[] = '<img src="'.base_url("./assets/img/GambarEvent/".$key["gambar_event"]).'"class="rounded-circle img-fluid" style="width: 100px;height:100px;">';
			// $data[] = ($key['is_active'] == '1') ? '<div class="badge badge-success">Active</div>' : '<div class="badge badge-danger">Deadactive</div>';
            $data[] = $key['deskripsi_event'];
            // $data[] = $key['gambar_event'];
            $data[] = $key['waktu_event'];
			$action = '';
			// if (check_rule(false,"is_update")) {
				$action .= '<a href="'.base_url("event/update/".$key['id_event']).'" class="badge badge-dark">
								<i class="mt-1 mr-1 mb-1 ml-1 fas fa-edit text-light"></i>
							</a>&nbsp';	
			// }
			// if (check_rule(false,"is_delete")) {
				$action .= '<a href="'.base_url("event/delete/".$key['id_event']).'" class="badge badge-danger">
								<i class="mt-1 mr-1 mb-1 ml-1 fas fa-trash text-light"></i>
							</a>';
			// }
			$data[] = $action;
			array_push($show, $data);
		}

		$data = [
			"draw" => post("draw"),
			"data" => $show,
			"recordsFiltered" => $this->EventModel->get_filtered_data(),
			"recordsTotal" => $this->EventModel->get_all_data()
		];
		echo json_encode($data, JSON_PRETTY_PRINT);
		// echo json_encode($_POST, JSON_PRETTY_PRINT);
	}
	
	public function tambah()
	{
		has_loggedin();
		// check_rule(false,"is_create",true);
		set_rules('nama_event', 'nama_event', 'required');
		set_rules('deskripsi_event', 'deskripsi_event', 'required');
		set_rules('waktu_event', 'waktu_event', 'required');
        set_rules('id_ormawa', 'id_ormawa', 'required');
		if (empty($_FILES['berkas']['name']))
		{
			$this->form_validation->set_rules('berkas', 'Gambar', 'required');
		}
		// set_rules('berkas', 'berkas', 'required');
		// set_rules('profile_type', 'profile type', 'required');
		
		if ($this->form_validation->run() == False) {	
			$data["title"] = "EVEJA";
			$data["ormawa"] = dbget('ormawa')->result_array();
			// $data["profile_style"] = rawview("templates/profile_style");
			$data["sidebar_style"] = rawview("templates/sidebar_style_ormawa");
			$data["topbar_style"] = rawview("templates/topbar_style", $data);
			$data["content"] = rawview("event/tambah", $data);
			// // // // $data["footer_style"] = rawview("templates/footer_style");
			view('templates/dashboard_style', $data);
		} else {
            $rand = rand();
            $file = $_FILES['berkas']['name'];
			$pathinfo = pathinfo($file);
			$extension = strtolower($pathinfo['extension']);
            $logo = $rand.'.'.$extension;
            $config['upload_path']   = './assets/img/GambarEvent/'; 
            $config['allowed_types'] = 'svg|jpg|png|jpeg'; 
            $config['max_size']      = 5120;
            $config['file_name'] = $logo;
            // $this->load->library('upload', $config);
			$this->upload->initialize($config);


            if (!$this->upload->do_upload('berkas')) {
                $data["error"] = array('error' => $this->upload->display_errors());
                $data["title"] = "EVEJA";
                // $data["role"] = dbget('role')->result_array();
                // $data["profile_style"] = rawview("templates/profile_style");
                $data["sidebar_style"] = rawview("templates/sidebar_style_ormawa");
                $data["topbar_style"] = rawview("templates/topbar_style", $data);
                $data["content"] = rawview("event/tambah", $data);
                // // // // $data["footer_style"] = rawview("templates/footer_style");
                view('templates/dashboard_style', $data);
            }else {
                // $ormawa = dbgetwhere("ormawa", [ => 2])->row_array();
				// $email = dbgetwhere("users", ["email" => post('email')])->row_array();
                $data= [
                    "id_ormawa" => post("id_ormawa"),
                    "nama_event" => post("nama_event"),
                    "deskripsi_event" => str_replace("\r\n", "<br>", post('deskripsi_event')),
                    "gambar_event" => $logo,
					"waktu_event" => post('waktu_event')
                ];
				dbinsert('event', $data);
                set_flashdata("msg", swalfire('Data Berhasil Dimasukkan', 'success'));
                redirect(base_url("event"));
            } 
		}
	}
	
	public function update($id)
	{
		has_loggedin();
		// check_rule(false,"is_update",true);

		set_rules('nama_event', 'nama_event', 'required');
		set_rules('deskripsi_event', 'deskripsi_event', 'required');
        set_rules('id_ormawa', 'id_ormawa', 'required');
		// set_rules('password', 'password', 'required');
		if ($this->form_validation->run() == False) {
			$data = [
				"title" => "EVEJA",
				"data" => $this->EventModel->get_user_by(['id_event' => $id]),
				"ormawa" => dbget("ormawa")->result_array()
			];
			// dd($data["user"]);
		$data["profile_style"] = rawview("templates/profile_style");
		$data["sidebar_style"] = rawview("templates/sidebar_style_ormawa");
		$data["topbar_style"] = rawview("templates/topbar_style");
		$data["content"] = rawview("event/update", $data);
		// // // // $data["footer_style"] = rawview("templates/footer_style");
		view('templates/dashboard_style', $data);
		} else {
			if(post('waktu_event') == ""){
				$waktu = post('waktu_event1');
			}
			else{
				$waktu = post("waktu_event");
			}
			if (!isset($_FILES['berkas'])){
				$data = [
					"id_ormawa" => post("id_ormawa"),
                    "nama_event" => post("nama_event"),
                    "deskripsi_event" => str_replace("\r\n", "<br>", post('deskripsi_event')),
                    "gambar_event" => post('berkas1'),
					"waktu_event" => $waktu
				];
				$this->EventModel->update($data, ['id_event' => $id]);
				set_flashdata("msg", "<script>Swal.fire('Success','Data Berhasil Diubah', 'success')</script>");
				redirect(base_url("event"));
			}
			else{
				$rand = rand();
				$file = $_FILES['berkas']['name'];
				$pathinfo = pathinfo($file);
				$extension = strtolower($pathinfo['extension']);
				$file = $rand.'.'.$extension;
				$config['upload_path']   = './assets/img/GambarEvent/'; 
				$config['allowed_types'] = 'svg|jpg|png|jpeg'; 
				$config['max_size']      = 5120;
				$config['file_name'] = $file;
				// $this->load->library('upload', $config);
				$this->upload->initialize($config);

				if (!$this->upload->do_upload('berkas')) {
					$data = [
						"title" => "EVEJA",
						"data" => $this->EventModel->get_user_by(['id_model' => $id]),
						"ormawa" => dbget("ormawa")->result_array()
					];
					// dd($data["user"]);
				$data["profile_style"] = rawview("templates/profile_style");
				$data["sidebar_style"] = rawview("templates/sidebar_style");
				$data["topbar_style"] = rawview("templates/topbar_style");
				$data["content"] = rawview("ormawa/update", $data);
				// // // // $data["footer_style"] = rawview("templates/footer_style");
				view('templates/dashboard_style', $data);
				}
				$data2 = [
					"id_ormawa" => post("id_ormawa"),
                    "nama_event" => post("nama_event"),
                    "deskripsi_event" => str_replace("\r\n", "<br>", post('deskripsi_event')),
                    "gambar_event" => $file,
					"waktu_event" => $waktu
				];
				$this->EventModel->update($data2, ['id_event' => $id]);
				set_flashdata("msg", "<script>Swal.fire('Success','Data Berhasil Diubah', 'success')</script>");
				redirect(base_url("event"));
			}
		}
	}
	public function delete($id)
	{
		has_loggedin();
		// check_rule(false,"is_delete",true);

		dbdelete('event', ['id_event' => $id]);
		set_flashdata("msg", swalfire('Data Berhasil Dihapus', 'success'));
		redirect(base_url("event"));
	}

}
