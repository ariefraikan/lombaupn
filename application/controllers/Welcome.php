<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
    }

    public function index()
    {
        view("auth/login");
    }

    public function login()
    {
        has_loggedin();

        set_rules("Username", "Username", "required");
        set_rules("Password", "Password", "required");
        
        if ($this->form_validation->run() == False) {
            $data['title'] = "Login";
            $this->load->view('templates/auth_header', $data);
            $this->load->view('auth/login');
            $this->load->view('templates/auth_footer');
        } else {
            $data = [
                "username" => post("Username"),
                "password" => post("Password")
            ];
            $dbget = dbgetwhere("user", ['Username' => $data['username']])->row_array();
            if ($dbget) {
                if (password_verify($data['password'], $dbget['Password'])) {
                    set_userdata("username", $dbget['Username']);
                    set_userdata("role_id", $dbget['IDRole']);
                    redirect(base_url('dashboard'));
                } else {
                    set_flashdata("msg", swalfire("Password Salah!","error"));
                    redirect(base_url('auth'));
                }
            } else {
                set_flashdata("msg", swalfire("User Tidak Terdaftar!","error"));
                redirect(base_url('auth'));
            }
        }
    }
    
    // public function registration()
    // {
    //     $this->form_validation->set_rules('name', 'Name', 'required|trim');
    //     $this->form_validation->set_rules('email', 'email', 'required|trim|valid_email');
    //     $this->form_validation->set_rules('password1', 'password', 'required|trim|min_length[3]|matches[password2]',['matches' => 'password dont match!', 'min_length' => 'password too short!']);
    //     $this->form_validation->set_rules('password1', 'password', 'required|trim|matches[password1]');
        
    //     if ($this->form_validation->run() == false)
    //     {
    //         $data['title'] = 'Registration Page';
    //         $this->load->view('templates/auth_header', $data);
    //         $this->load->view('auth/registration');
    //         $this->load->view('templates/auth_footer');
    //     }
    //     else 
    //     {
    //        $data = [
    //            'Username' => $this->input->post('name'),
    //            'Password' => password_hash($this->input->post('password'), PASSWORD_DEFAULT),
    //            'IDRole' => 1,
    //        ];

    //        $this->db->insert('user', $data);
    //        redirect('auth');
    //     }
    // }

    public function forbidden()
    {
        view("forbidden");
    }

    public function logout()
    {
        unset_userdata("username");
        unset_userdata("role_id");
        set_flashdata("msg", swalfire("Logout Berhasil", "success"));
        redirect(base_url("auth"));
    }
}

