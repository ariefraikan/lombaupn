<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DashboardOrmawa extends CI_controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
    }
    public function index()
    {
        has_loggedin();
        $user = dbgetwhere("users",["id_role" => 3])->result_array();
        $ormawa = dbget("ormawa")->result_array();
        $event = dbget("event")->result_array();
        $data["user"] = count($user);
        $data["ormawa"] = count($ormawa);
        $data["event"] = count($event);
		$data["title"] = "EVEJA";
		$data["sidebar_style"] = rawview("templates/sidebar_style_ormawa");
		$data["topbar_style"] = rawview("templates/topbar_style");
		$data["content"] = rawview("dashboardormawa/index", $data);
		view('templates/dashboard_style', $data);
    }
}