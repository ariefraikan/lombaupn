<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model("UserModel");
	}

	public function index()
	{
		has_loggedin();
		// dd(userdata('role_id'));
		// check_rule(false,"is_read",true);
		$data["title"] = "EVEJA";
		// $data["profile_style"] = rawview("templates/profile_style");
		$data["sidebar_style"] = rawview("templates/sidebar_style");
		$data["topbar_style"] = rawview("templates/topbar_style", $data);
		$data["content"] = rawview("user/index");
		// // // // // $data["footer_style"] = rawview("templates/footer_style");
		view('templates/dashboard_style', $data);
	}

	public function datatable()
	{
		// check_rule(false,"is_read",true);

		$show = [];
		
		// $order_column = post("order")[0]['column'] == 0;
		// $order_dir = post("order")[0]['dir'] == "asc";
		// $i = ($order_column && $order_dir) ? post("start") + 1 : $this->UserModel->get_filtered_data() - post("start");
		foreach ($this->UserModel->datatable() as $key) {
			$data = [];
			// $data[] = ($order_column && $order_dir) ? $i++ : $i--;
			$data[] = $key['nama'];
			$data[] = $key['email'];
			$data[] = $key['nama_role'];
			$data[] = ($key['is_active'] == '1') ? '<div class="badge badge-success">Active</div>' : '<div class="badge badge-danger">Deadactive</div>';
			$action = '';
			// if (check_rule(false,"is_update")) {
				$action .= '<a href="'.base_url("user/update/".$key['id_user']).'" class="badge badge-dark">
								<i class="mt-1 mr-1 mb-1 ml-1 fas fa-edit text-light"></i>
							</a>&nbsp';	
			// }
			// if (check_rule(false,"is_delete")) {
				$action .= '<a href="'.base_url("user/delete/".$key['id_user']).'" class="badge badge-danger">
								<i class="mt-1 mr-1 mb-1 ml-1 fas fa-trash text-light"></i>
							</a>';
			// }
			$data[] = $action;
			array_push($show, $data);
		}

		$data = [
			"draw" => post("draw"),
			"data" => $show,
			"recordsFiltered" => $this->UserModel->get_filtered_data(),
			"recordsTotal" => $this->UserModel->get_all_data()
		];
		echo json_encode($data, JSON_PRETTY_PRINT);
		// echo json_encode($_POST, JSON_PRETTY_PRINT);
	}
	
	public function tambah()
	{
		has_loggedin();
		// check_rule(false,"is_create",true);
		set_rules('nama', 'nama', 'required');
		set_rules('email', 'email', 'required|valid_email');
		set_rules('password', 'password', 'required');
		set_rules('id_role', 'role', 'required');
		// set_rules('profile_type', 'profile type', 'required');
		
		if ($this->form_validation->run() == False) {
			$data["title"] = "EVEJA";
			$data["role"] = dbget('role')->result_array();
			// $data["profile_style"] = rawview("templates/profile_style");
			$data["sidebar_style"] = rawview("templates/sidebar_style");
			$data["topbar_style"] = rawview("templates/topbar_style", $data);
			$data["content"] = rawview("user/tambah", $data);
			// // // // $data["footer_style"] = rawview("templates/footer_style");
			view('templates/dashboard_style', $data);
		} else {
			$email = dbgetwhere("users", ["email" => post('email')])->row_array();
				if($email != NULL){
					set_flashdata("msg", swalfire('Email Sudah Terdaftar', 'error'));
                	redirect(base_url("user"));
				}
			$data = [
				"nama" => post('nama'),
				"email" => post('email'),
				"password" => password_hash(post('password'), PASSWORD_DEFAULT),
				"id_role" => post('id_role'),
				"is_active" => 1,
				"created_at" => date("Y-m-d H:i:s"),
			];
			dbinsert('users',$data);
			set_flashdata("msg", swalfire('Data Berhasil Dimasukkan', 'success'));
			redirect(base_url("user"));
		}
	}
	
	public function update($id)
	{
		has_loggedin();
		// check_rule(false,"is_update",true);

		set_rules('nama', 'nama', 'required');
		// set_rules('password1', 'password1', 'required');
		set_rules('id_role', 'role', 'required');

		if ($this->form_validation->run() == False) {
			$data = [
				"title" => "EVEJA",
				"data" => $this->UserModel->get_user_by(['id_user' => $id]),
				"role" => dbget("role")->result_array()
			];
		$data["profile_style"] = rawview("templates/profile_style");
		$data["sidebar_style"] = rawview("templates/sidebar_style");
		$data["topbar_style"] = rawview("templates/topbar_style");
		$data["content"] = rawview("user/update", $data);
		// // // // $data["footer_style"] = rawview("templates/footer_style");
		view('templates/dashboard_style', $data);
		} else {
			if(post('password') == ""){
				$encrypt = post('password1');
			}
			else{
				$encrypt = password_hash(post('password'), PASSWORD_DEFAULT);
			}
			$data = [
				"nama" => post('nama'),
				"password" => $encrypt,
				"id_role" => post('id_role')
			];
			$this->UserModel->update($data, ['id_user' => $id]);
			set_flashdata("msg", "<script>Swal.fire('Success','Data Berhasil Diubah', 'success')</script>");
			redirect(base_url("user"));
		}
	}
	public function delete($id)
	{
		has_loggedin();
		// check_rule(false,"is_delete",true);

		dbdelete('users', ['id_user' => $id]);
		set_flashdata("msg", swalfire('Data Berhasil Dihapus', 'success'));
		redirect(base_url("user"));
	}

}
