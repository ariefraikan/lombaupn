<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ormawa extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model("OrmawaModel");
		$this->load->model("UserModel");
        $this->load->helper('file');
        $this->load->library('upload');
	}

	public function index()
	{
		has_loggedin();
		// dd(ormawadata('role_id'));
		// check_rule(false,"is_read",true);
		$data["title"] = "EVEJA";
		// $data["profile_style"] = rawview("templates/profile_style");
		$data["sidebar_style"] = rawview("templates/sidebar_style");
		$data["topbar_style"] = rawview("templates/topbar_style", $data);
		$data["content"] = rawview("ormawa/index");
		// // // // // $data["footer_style"] = rawview("templates/footer_style");
		view('templates/dashboard_style', $data);
	}

	public function datatable()
	{
		// check_rule(false,"is_read",true);

		$show = [];
		
		// $order_column = post("order")[0]['column'] == 0;
		// $order_dir = post("order")[0]['dir'] == "asc";
		// $i = ($order_column && $order_dir) ? post("start") + 1 : $this->ormawaModel->get_filtered_data() - post("start");
		foreach ($this->OrmawaModel->datatable() as $key) {
			$data = [];
			// $data[] = ($order_column && $order_dir) ? $i++ : $i--;
			$data[] = $key['nama_ormawa'];
			$data[] = '<img src="'.base_url("./assets/img/LogoOrmawa/".$key["logo_ormawa"]).'"class="rounded-circle img-fluid" style="width: 100px;height:100px;">';
			$data[] = ($key['is_active'] == '1') ? '<div class="badge badge-success">Active</div>' : '<div class="badge badge-danger">Deadactive</div>';
			$action = '';
			// if (check_rule(false,"is_update")) {
				$action .= '<a href="'.base_url("ormawa/update/".$key['id_ormawa']).'" class="badge badge-dark">
								<i class="mt-1 mr-1 mb-1 ml-1 fas fa-edit text-light"></i>
							</a>&nbsp';	
			// }
			// if (check_rule(false,"is_delete")) {
				$action .= '<a href="'.base_url("ormawa/delete/".$key['id_ormawa']).'" class="badge badge-danger">
								<i class="mt-1 mr-1 mb-1 ml-1 fas fa-trash text-light"></i>
							</a>';
			// }
			$data[] = $action;
			array_push($show, $data);
		}

		$data = [
			"draw" => post("draw"),
			"data" => $show,
			"recordsFiltered" => $this->OrmawaModel->get_filtered_data(),
			"recordsTotal" => $this->OrmawaModel->get_all_data()
		];
		echo json_encode($data, JSON_PRETTY_PRINT);
		// echo json_encode($_POST, JSON_PRETTY_PRINT);
	}
	
	public function tambah()
	{
		has_loggedin();
		// check_rule(false,"is_create",true);
		set_rules('nama_ormawa', 'nama_ormawa', 'required');
		set_rules('email', 'email', 'required|valid_email');
		set_rules('password', 'password', 'required');
		if (empty($_FILES['userfile']['name']))
		{
			$this->form_validation->set_rules('userfile', 'Document', 'required');
		}
		// set_rules('berkas', 'berkas', 'required');
		// set_rules('profile_type', 'profile type', 'required');
		
		if ($this->form_validation->run() == False) {	
			$data["title"] = "EVEJA";
			// $data["role"] = dbget('role')->result_array();
			// $data["profile_style"] = rawview("templates/profile_style");
			$data["sidebar_style"] = rawview("templates/sidebar_style");
			$data["topbar_style"] = rawview("templates/topbar_style", $data);
			$data["content"] = rawview("ormawa/tambah", $data);
			// // // // $data["footer_style"] = rawview("templates/footer_style");
			view('templates/dashboard_style', $data);
		} else {
            $rand = rand();
            $file = $_FILES['berkas']['name'];
			$pathinfo = pathinfo($file);
			$extension = strtolower($pathinfo['extension']);
            $logo = $rand.'.'.$extension;
            $config['upload_path']   = './assets/img/LogoOrmawa/'; 
            $config['allowed_types'] = 'svg|jpg|png'; 
            $config['max_size']      = 5120;
            $config['file_name'] = $logo;
            // $this->load->library('upload', $config);
			$this->upload->initialize($config);


            if (!$this->upload->do_upload('berkas')) {
                $data["error"] = array('error' => $this->upload->display_errors());
                $data["title"] = "EVEJA";
                // $data["role"] = dbget('role')->result_array();
                // $data["profile_style"] = rawview("templates/profile_style");
                $data["sidebar_style"] = rawview("templates/sidebar_style");
                $data["topbar_style"] = rawview("templates/topbar_style", $data);
                $data["content"] = rawview("ormawa/tambah", $data);
                // // // // $data["footer_style"] = rawview("templates/footer_style");
                view('templates/dashboard_style', $data);
            }else {
                $role = dbgetwhere("role", ["id_role" => 2])->row_array();
				$email = dbgetwhere("users", ["email" => post('email')])->row_array();
				if($email != NULL){
					set_flashdata("msg", swalfire('Email Sudah Terdaftar', 'error'));
                	redirect(base_url("ormawa"));
				}
                $data = [
                    "nama" => post('nama_ormawa'),
                    "email" => post('email'),
                    "password" => password_hash(post('password'), PASSWORD_DEFAULT),
                    "id_role" => $role["id_role"],
                    "is_active" => 1,
                    "created_at" => date("Y-m-d H:i:s"),
                ];
                dbinsert('users',$data);
				$last_id = dbinsertid();
                $data2= [
                    "nama_ormawa" => post("nama_ormawa"),
                    "logo_ormawa" => $logo,
                    "is_active" => 1,
					"id_user" => $last_id
                ];
				dbinsert('ormawa', $data2);
                set_flashdata("msg", swalfire('Data Berhasil Dimasukkan', 'success'));
                redirect(base_url("ormawa"));
            } 
		}
	}
	
	public function update($id)
	{
		has_loggedin();
		// check_rule(false,"is_update",true);

		set_rules('nama_ormawa', 'nama_ormawa', 'required');
		set_rules('email', 'email', 'required|valid_email');
		// set_rules('password', 'password', 'required');
		if ($this->form_validation->run() == False) {
			$account = $this->OrmawaModel->get_user_by(['id_ormawa' => $id]);
			$data = [
				"title" => "EVEJA",
				"data" => $this->OrmawaModel->get_user_by(['id_ormawa' => $id]),
				"user" => dbgetwhere("users",["id_user" => $account['id_user']])->result_array()
			];
			// dd($data["user"]);
		$data["profile_style"] = rawview("templates/profile_style");
		$data["sidebar_style"] = rawview("templates/sidebar_style");
		$data["topbar_style"] = rawview("templates/topbar_style");
		$data["content"] = rawview("ormawa/update", $data);
		// // // // $data["footer_style"] = rawview("templates/footer_style");
		view('templates/dashboard_style', $data);
		} else {
			$account = $this->OrmawaModel->get_user_by(['id_ormawa' => $id]);
			if(post('password') == ""){
				$encrypt = post('password1');
			}
			else{
				$encrypt = password_hash(post('password'), PASSWORD_DEFAULT);
			}
			$data = [
				"nama" => post('nama_ormawa'),
				"password" => $encrypt,
			];
			$this->UserModel->update($data, ['id_user' => $account['id_user']]);
			if (!isset($_FILES['berkas'])){
				$data2 = [
					"nama_ormawa" => post("nama_ormawa"),
					"logo_ormawa" => post('berkas1'),
					"is_active" => 1
				];
				$this->OrmawaModel->update($data2, ['id_ormawa' => $id]);
				set_flashdata("msg", "<script>Swal.fire('Success','Data Berhasil Diubah', 'success')</script>");
				redirect(base_url("ormawa"));
			}
			else{
				$rand = rand();
				$file = $_FILES['berkas']['name'];
				$pathinfo = pathinfo($file);
				$extension = strtolower($pathinfo['extension']);
				$file = $rand.'.'.$extension;
				$config['upload_path']   = './assets/img/LogoOrmawa/'; 
				$config['allowed_types'] = 'svg|jpg|png|jpeg'; 
				$config['max_size']      = 5120;
				$config['file_name'] = $file;
				// $this->load->library('upload', $config);
				$this->upload->initialize($config);

				if (!$this->upload->do_upload('berkas')) {
					$data = [
						"title" => "EVEJA",
						"data" => $this->OrmawaModel->get_user_by(['id_ormawa' => $id]),
						"user" => dbgetwhere("users",["id_user" => $account['id_user']])->result_array()
					];
					// dd($data["user"]);
				$data["profile_style"] = rawview("templates/profile_style");
				$data["sidebar_style"] = rawview("templates/sidebar_style");
				$data["topbar_style"] = rawview("templates/topbar_style");
				$data["content"] = rawview("ormawa/update", $data);
				// // // // $data["footer_style"] = rawview("templates/footer_style");
				view('templates/dashboard_style', $data);
				}
				$data3 = [
					"nama_ormawa" => post("nama_ormawa"),
					"logo_ormawa" => $file,
					"is_active" => 1
				];
				$this->OrmawaModel->update($data3, ['id_ormawa' => $id]);
				set_flashdata("msg", "<script>Swal.fire('Success','Data Berhasil Diubah', 'success')</script>");
				redirect(base_url("ormawa"));
			}
		}
	}
	public function delete($id)
	{
		has_loggedin();
		// check_rule(false,"is_delete",true);

		dbdelete('ormawa', ['id_ormawa' => $id]);
		set_flashdata("msg", swalfire('Data Berhasil Dihapus', 'success'));
		redirect(base_url("ormawa"));
	}

}
