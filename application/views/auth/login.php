<?= flashdata("msg");set_flashdata("msg", "") ?>
<!-- Navigation-->

<body class="hold-transition login-page">
	<div class="login-box">
		<div class="login-logo">
			<div class="text-center">
				<img src="<?php base_url(); ?> assets/img/LogoEveja.png" alt="Eveja Logo" style="width: 10rem;"></a>
			</div>
			<!-- <img src="<?php base_url(); ?> assets/img//LogoEveja.png" alt="Eveja Logo" style="width: 4rem;"></a> -->
			<b>Admin</b>
		</div>
		<!-- /.login-logo -->
		<div class="card">
			<div class="card-body login-card-body">
				<p class="login-box-msg">Silahkan masukan data anda</p>

				<form action="" method="post">
					<div class="input-group mb-3">
						<input type="email" class="form-control" placeholder="Email" name = 'email'  id='email' value="" required>
						<div class="input-group-append">
							<div class="input-group-text">
								<span class="fas fa-envelope"></span>
							</div>
						</div>
					</div>
					<div class="input-group mb-3">
						<input type="password" class="form-control" placeholder="Password" name='password'  id='password' required>
						<div class="input-group-append">
							<div class="input-group-text">
								<span class="fas fa-lock"></span>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-12">
							<input type="submit" class="btn btn-primary btn-block">
						</div>
						<!-- /.col -->
						<!-- /.col -->
					</div>

				<!-- <div class="social-auth-links text-center mb-3">
        <p>- OR -</p>
        <a href="#" class="btn btn-block btn-primary">
          <i class="fab fa-facebook mr-2"></i> Sign in using Facebook
        </a>
        <a href="#" class="btn btn-block btn-danger">
          <i class="fab fa-google-plus mr-2"></i> Sign in using Google+
        </a>
      </div> -->
				<!-- /.social-auth-links -->

				<!-- <p class="mb-1">
					<a href="forgot-password.html">I forgot my password</a>
				</p>
				<p class="mb-0">
					<a href=" base_url();  ?> daftar" class="text-center">Register a new membership</a>
				</p> -->
			</div>
			<!-- /.login-card-body -->
		</div>
	</div>
	<!-- /.login-box -->

	<!-- jQuery -->
	<script src="../../plugins/jquery/jquery.min.js"></script>
	<!-- Bootstrap 4 -->
	<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
	<!-- AdminLTE App -->
	<script src="../../dist/js/adminlte.min.js"></script>
</body>
