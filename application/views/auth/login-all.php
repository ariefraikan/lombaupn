<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />

		<!-- Bootstrap CSS -->
		<link
			href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"
			rel="stylesheet"
			integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC"
			crossorigin="anonymous"
		/>

		<!-- My CSS -->
		<link rel="stylesheet" href="../../../../assets/assets/css/testlogin.css" />
		<title>Login</title>
	</head>
	<body>
		<div class="container-fluid">
			<div class="row">
				<div id="colgrad" class="col text-center">
					<div id="logo" class="container">
						<svg
							width="200"
							height="90"
							viewBox="0 0 200 90"
							fill="none"
							xmlns="http://www.w3.org/2000/svg"
							xmlns:xlink="http://www.w3.org/1999/xlink"
						>
							<rect width="200" height="89.0966" fill="url(#pattern0)" />
							<defs>
								<pattern
									id="pattern0"
									patternContentUnits="objectBoundingBox"
									width="1"
									height="1"
								>
									<use
										xlink:href="#image0_63:273"
										transform="scale(0.00311526 0.00699301)"
									/>
								</pattern>
								<image
									id="image0_63:273"
									width="321"
									height="143"
									xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAUEAAACPCAYAAABkm2lBAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAABR7SURBVHgB7d1fbFvXfQfw37n30k7TDGBiK/uHIRQsAUPrpDIwqXbW2NTT9hb3bXsyjWFA+1Bbehiwh2Gm0W5AhwGykgF7WDDTexk2YKj70DZ+EhW3tWtnsBp7wwbbELOlnhM7Dge7iy1e3rNzLkmJEs+5vOf+Iyl+P4BhWSLlf9RPv/Pve4gAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEbWjfVCnkbIqP15AbJgEURy68Mvn3Dohe/RCMnRF5durh88QwCwiREYu/nhwdPk0Tn5tuvS/KHpW1UacjfWf7vgkLPe/mX51clbZwkA0Ama+rcPRSfVLoCS49BIdFY5srv/nGV0hAAt6AQNyALoeVTe+f5h7wZ3dIHd0BHC2EMnaEAUwKLq/cPeDe7oAjcxYiUCGHMogmZ0XVPxxu2DRRpCsgvkmmLn6f8+AGMDRdCAGDpWxU9V1cdyDj9PQ0gMg5V/LtEF1l6bvFkhgDGHIhjS0o2iv8fOJeek6uOi2yp8cOdLJRoiYvGjKH4qqj7WcPlJAgAUwTD+5mdHT9jPmv6ewEOTazXO2QXV45htLd1YnxmaDcmM1N0p57TavZDz1tU3VpauHC4QwBhCEezj7atHT3ucV8SbxaUrXyvK9zWZvSB+qisenrc8d4GGgOxKZXeq+liTOaXO2/LvJ34q2iyHQghjKXCLTH6mkKfnaGSOWtWv1mqUoLd/dvQM57zc9a7qqcOX5+UbYqgp369ada2LIfOk6BjrNEC31r+8riqCYi6wcnDypj8UXrpSLNjMWxG9Yedx9WazOb/4uz9do4SM+2soSZH/LZ9Svb5Wy/z1GOf/Psv/Byfog7lc7jh5wznhr5HYvkdFAZT8bnDxyI+rotCdc8iVXdTO/+RON1imAQnqAhtkb64IO5Z3Qvwdux+Xt217ZeknrydWCMf5NZS0yP+We/2RTOZzwM4eZ4V5NEPm6qKATmZVuDEc1hDFoah6v82Y3/21O71l1WMsi04Pcm7QspluX2BFzmnKt2UXqCjyUt6yrOMEEIMoYgXx3SRKAfSfbufszF6DKIIaTc61ewI7c4OyGyTN3GCOGgPZQC3PNYfpAm3W1P35aqeP/LhMADE4jhOriFnETlBGUAQ15JCXNHsCRTfoD0mCukFRiBbkRmXKkPz9mMeVCzM7u0DxU0n5STg2UEN8zGKnKQ5GRX9OMQMoggGa3NbNoxTeuvJGSb7x6uStstx4rHqQ7rhaWvZYzgnNYkgtbBd46sjlCgHEkJ+dnulabIvM2bu1iyFNKIIBFo9Ua5yTck+gqCxLnQ3UuuNn8rhaVsfpZBeoCneQmh6/gC4QsmKTV6IEiEWVNykDKIJ9rFx6XCbdnsCnrT2B7eNnVdXzswpXCAhJqH3lwK1y59foAiFtFrOSKV4ZDYlRBANMzB048ZMfPDkvVlGV836MsdOdbpAGGK4QNiShvaCjfJzsAidmp8/LvzMBRLT/8FQxiaFwRxZDYhRBDVEQxMQuq8jvRu/89adyz1xgNxgUrpB2NxjUBXaHJHS29/TgtPrn3/zvKjFekn/nX/3q1FCceoHRw5rJrupmMSRGEVR4eW7qjCgIm+nR9/+rcXpYu8GwXWB7IaeoehwjXs7t3Uqb8Tgt+f8GAKaY+jUW5/PlX598hVLkUDpWaUTJL36+87RHqxtc/uM/2S+7wZ4TIvYzTxaMRdkN3lw/WCVFsWl3g1VKmOwCueL9sgt8tTsqy+8CVY+kytnF+6Lmb/8zy38D8W9Bn1y7M6jFkpF9DY0rfyjsJTcU7nBcf5dGmVKSShF8cO1OkUYUl9/JFLWi0w0y5ZCSLyxdOby8eORqjVrdV1HxqYvyONtrU/9eoYTIqCyumePr7QLVL84mb5xlHltRFUixMi7PSQ+kCI7ya2hc+UNhxsM9mIvpJRbuXLEYqcghdplSguHwDqIIqr/oRXH89rfuV0mzJ9BmOb84ym5QF7WlO84Wg/Lzyais13q6QKXKX576tKgrkLbtlgggLKOhMFs0eGxh/+/IBZd0oAju8PDqHbFAoB62MoufJ/1xulJX1FZZ9QC5kfnndw+WKQHtANei6mPN5tZ3zX5doPi7qgsk45X7Q5yoAsNl39yB46FXhcU36QfXb1c4p/ABCVbCc43bPjX0sJmrOSnCCt8+dU++UVU/bzNcoUaaYWRS4QqBIQntwFR/wSZqF8ia2DgNoVk8/N5AbtFF+bOYWvp+2OcwonjH8AKgCCq0OiCmOSnClv7no2fLmqeGCleIG7waOiprwzuNLhCywImHDkwQc9DV9pOqFF4+rSExiqBGY6OxoGnX83/3V49kRFBV9bwsorbCR2WpF03EAs9Z0QW+iS4QkjAxO11i4Rc5amIo7GdVNhqNi2SAsfCF1gSKoIYf6MjURUy25oPqBsN2ga3jccoiV7t86Ymc91T//ugCwRBnPPxQuGu+vf01Vg37XFEFUznNhCIYwN1wz6XbDZpFbd1YL+TDdoEUcDzuvR8+OaYqkPK7NLpAMCHP9oqmIPxQ2KVt84C8SaHnBSmlITGKYIA43WAnais4eNUsamuP9ULswFR5PI5r9lx5Yh4UXSCY8CP/DWw0m9XuXzuOM/AhMYpgH0Hd4DvffXQsIGprsxvkmj1RftRWyG7QPx7nybO9vTyPzobtAsWLVtNJ8trDa3fKBGDC4uGHqJxWd94bcv/qf9R0e2+VxJA46WQZFME+grpBOa92+dLjiuapheUrXyvLN+TGZV3wqkNbZ3aDtI7HsULvH4HVPMupdH7drwtshST04sgSBEPyHhHiJvv3/AufenjcMxoSi+5xhhKEIhiC7JB0RWz1B4/ljW3KAtIdrsCJ61Kq+4Yr9AtJSKILfHD9boUADIjXU9Hk8ba9fSjcwWxmOCRONpUJRTAkbafEWCntqK3wUVlNXVeJLhCSZzAUZpzWdPPN7lN3zej0CKOZJIfEKIIhyWM+uuX8NKO2DANTlZ+DcX4SXSAkyXQoLEZC2lQgOeXELFojg98+ySs5UQQNBIUrpNUNhu8CLXUXKCajZVQWukBIkulQmPPgIa/hVplEr+REETQQFK7Qtxu8crjQ/mXobtAsMFVzPI4aJTEU0SZKowuEKDjjJmd56w/fF187AVzXrZCJBO8fSSVPMH9YtMoZqme4t012g0w1DOgXvNqK2joZFLyac7js5iY7v9atHJsEpvohCZqII9EFlmlI7ebX0KiTQ2FGFH6FlvG+Cx9ySDzx1amqyRC7ff/IOYoplSKY85x1ylD+9clC/afrH1IGZDeo+8+S3aB4/6IoOqriVRLd4FkZvOqSc9Iht+ffSG6B6QSvysBU0szxNdytleYQUVkrqo/J43EP379bpSG1m19Do87ZY3j5kRcuJVzmYDIKXwTb94/ELoIYDkcQGLzqR21pg1f94ii3tOiCV5ltLclwBbFgoewC5QulE5XVegKisiBbzHA+Trc1podleP1EQkNiFMEI/LlBbdQWnWlyT7snsCt4VS6WaI7TNVZ0x+OabOu78NtXjyIqCzKVn52eCR2eKolv2mFfa/LrymirDCVzJSeKYES21SirP8IKf3H6fpFihCuIAqicb+mJyiLSJdGgC4RU5AzDTTsBqqEfT/yCyeOTuJITRTAi+d2Na1Z6E4jaUtoWmGp5J3RRWTf/9ekyukBIheGVmpsBqmEfb3h6JIkrOVEEY0gzamun3sBUXlY9Trz/wvcvfIbAVEicf6WmwVC4O0A1LOPTI7R5JWdkKIIxpBy8uk3YqKwz3/yoottbKDtXdIEQlX+lpgHODBc6KNLpkc6VnJGhCMYUoxvsrBT37QZNAlNbx+PUgamOZbghFaCb4VCYN5nR/N7W88xOj8S9khNFMKZ+3eB77z7WDT8LneDVVydvlUnfDdZNAlODjsehC4SoTIfCUr9TIjqmQau+GFdyoggmIKgbbEVtaYNXl2S4QjtYVbffKU+uKz+OqCwYGNOhsGgADLu5LcZBqxTvSs5UToyIZfF5ytCgd/r7R35mpxdJtcGZsdI//f2nh/7gj/bJpfye43QyXCFH9is84PO3wxWq/brA3B5Ht8F65BZDxu01NPRMh8Kcm3dzXWTQqsWYSWHz7x+J0n2mUgRbm4nHi4zaenlu+gxXDBlur20syXAFpjjdIcMVHjzNfX3iuWaRK4cbrC4K5HJ7IaWk/M235gJ7ny26wE9GsAscx9fQsNo3d+C46VA49CkRDX+rjGfW3bXvH6mSIQyHE+RZmvToPlFbH9RfKIp5v/ne9GpWd8X7X5u8edHWHY/jtIrAVEiTxS2jDclBAaphRTk9EvVKThTBBMWJ2qrW8/XthbBVAMWq8Fp7AaWofC7xcm6vLm0Gc4EQjzybK0YoRgGmQQGqJsTXhem8YqQrOVMZDo+zyFFbz7wzouAt3lifmbe5W2kyu9TZFhMUlSUDU5kmfsjzWKxNpAD+lZqMG4UUeERriUShNemG+LoxW5CJMCRGEUxYv6gt3dygKHILS1cOLx+avFqjrq6vX1QW89iKskCKYXLULQoAHZzxN5nZU0gsaJy3vARKC+NkrHUlZ3nn1Z5BMBxOQWDU1rfuVwOitnqLY9SoLNstEUAM7fDUxO7yyIjxlZwogikImhtkFj9Pmis6yQ9ebR2nk0IEpiIkYYyIrqxAEXDOIm3/Mb1HZFiYXsmJIpgSm7ma+ThWaAWvBocr+DfUITAVuoiplETu1AjNincmd2AMr+REEUxJqxPTBa+ypX7hCs6Gh8BU2May2FcoCma41YTMr9QcMkZXcqIIpqix0ViIEq5gWdaSeF5J9THRDZwVXSCissaQeE1E6gS5x42L4KgOhTtMruREEUxR1Kgtxrkuwrx2+dITOd+4QOonogvcpeTwzuiGty6Mm53D9Y3qULjD4P4RFMGURY3aUuJ09r0fPjmmi8pCF7h7Oc85kQqgxBgz6gRHfCi8Kez9IyiCKYsRvLqTH5LASX1XsCfmH9EF7l6mKS7dTNOdHccZtW0xSmHvH0ERzEBQN/jOdx8d00ZtdesTlfXw2p0ywe7FonVm8hwvmbJotIfCHSGHxDgxkgHZDe6fm5Idn2IzNC1cvvR4/ujv/0rQC2/XRWVBeBOz0yXTFJcObnGjItjeIG089G5Y7iSlKNfM5UVFv0GG2kPic4GPIciE7NRenps+oYrLksGrb/zeC2eZPilm10VlgQHDzb/beGQUZmDn7AUyJANU6xlMxeiOowZpD4kDiyCGwxnSdmyMlT7+qKHNTrv3i8YxRGUNH5MNuVG9PDd1JmoXKJnm+lnMLDZLihugGv73MSvovhBXcqIIZkgGr6qO070ytYd+7bf2aL+gXtpnl/Z+ofcYO6KyBkcWQDE98dnE7IHz/mpqCibmDpzQLYSFYZrrl5+dnolScOMGqIblchap2Pa7khNFMGOqcIWPf+GS6AS1z7lW/SU9+7x3KIwucHDsPXbRf0N08aIYrouO7XtxbjzbScwDipEBq1AMnhViwa1LLsI9HUkEqIZVF6vcxkGr1P9KThTBjKnCFZ5+7tE/vP2I/vODpz2Pf+9Hj2n1R096P5EYGqALHKAdc1PiW9RxZtGK6N7WRTFciNodykI6MTu1IqrLOYrJYa5Z5xRhBdpj0S9UikLMqRsV9pbgKzlTWRgRCwDrNACfXLud6gpVUlTBq7IQ/vM7n9EffuOl2tSX9hbk+66v/rImCmBB+Tl49GHSKBj215B/jlcZd8cKohguie5waWJuak08psoYX+Xcqqn268lhtYx+siySW6WKUbfC9P4xzE4P+VdqehHmHj3zOz3iiHL3iK91JWdV9aF0bpuLMZE7DoKCV//xbx/V/uytXxcTwLz27r88Vrfx4gX+8P27VdrFhvk15C+IhFulnJGJJpzYghw3iqJI3ffIeJznGWudB/brqWl6aQDT00P+ZmzDEFN/Tjrj16H71F1zck698+8WVvtKzrLqYxgOD0hQ8Op3Tt2rfufUxzWEJAynOEfYZHHv/DD9Qg7/m/Bl43m6CB0oZ9l2gZLccys6bfMN4AH3j6AIDkjrSkld1BadQVTW8OJNs4uHsiS7s8YXm2WT5/hD4QidN3OznQ/s4M1ov2/7/pEeKIIDZFuNsvojrIAucHhFzvVLm1g5tazmfL1aM1pBjXoueaOZzdaYnVzXrVAUmis5UQQHSHZ0YhYmfFFDFzgchjFhxd86wuajvD5Mr9RsP2nV5DKjJLVDSapkTjkkRhEcsIBwhR7oAgevNXQcLnIILAugaVqMtG/uwPFoc5O8QgMU6fQIqYfEKIIDFhS11U12jOgCB08MHQs0RMTr4uLG881DUQqgZHHzY3JSVqdEtKyIizKtKznz2z8VDFy/blB+p3esiPMgkCh59NH2E1NYhE27iVrlFs0/vHbn66ZzgB1+MdCcSQ+S5SkRHbmwGOX0CCmu5EQRHAL9ukGPsWV0gcND/l88uHa7JIuh7NAZRYivj84vfg+u3Sm2dhhEl8vlIq1yiznESEPRpDHGIq4Sb995gSI4JBhn2qQL5vHhXI0cc7IYyog0ecpEFibxnyi/kf2ckuR3O/wCcXay8bz7YhLFb/NTMx5pKMwjBhkkjkcdEm+/kjPBPeoQlTxnKg/hBz2mseG+OKjVODCTLxbyzlMx5HJphtm8IKqNHH7Ji5LyonPc+mYnCpzoSv639SbVxY+amPqoc4+tcYvXmlZzrY4RQOpQBIfExOx0RXQS6v1anF0Qc1ElAoDE2QRD4f/uPbr4hd948UUxz3F42wc4X35w/c43CABSgSI4RD6/99m7z//mS2KE1NqMKyfdH16/+6cEADBO9s9NleUPAgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAANhV/h9D/tIcFXgllgAAAABJRU5ErkJggg=="
								/>
							</defs>
						</svg>
					</div>
				</div>
				<div class="col g-5 g-xl-8 text-center justify-content-center">
					<div class="row">
						<div id="loginheading" class="container">
							<h1>Masuk</h1>
						</div>
					</div>
					<div class="row">
						<div class="container text-center">
							<input
								type="email"
								class="form-control-lg"
								id="formemail"
								placeholder="E-mail"
							/>
						</div>
					</div>
					<div class="row">
						<div class="container">
							<input
								type="password"
								class="form-control-lg"
								id="formpassword"
								placeholder="Kata Sandi"
							/>
						</div>
					</div>
					<div class="row">
						<div class="container">
							<button type="submit" id="btnlogin" class="btn bordergrad">
								Masuk
							</button>
						</div>
					</div>
					<div class="row">
						<div id="forgetpass" class="container">
							<a href="#">
								<h6>Lupa kata sandi?</h6>
							</a>
						</div>
					</div>
					<div class="row">
						<div id="signup" class="container">
							<h6>Tidak punya akun? <a href="#">Buat akun</a></h6>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>
