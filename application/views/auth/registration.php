<?= flashdata("msg");set_flashdata("msg", "") ?>
<div class="container">

    <div class="card o-hidden border-0 shadow-lg my-5 col-lg-7 mx-auto">
        <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
                <div class="col-lg">
                    <div class="p-5">
                        <div class="text-center">
                            <h1 class="h4 text-gray-900 mb-4">Buat Akun!</h1>
                        </div>
                        <form class="form-control-user" method="post" action="">
                            <div class="mb-3">
                                <div class="form-group">
                                    <input type="text" class="form-control form-control-user" id="nama" name="nama" placeholder="Nama Lengkap">
                                    <?= form_error('nama', '<small class="text-danger pl-3">', '</small>'); ?>
                                </div>
                            </div>
                            <div class="mb-3">
                                <div class="form-group">
                                    <input type="email" class="form-control form-control-user" id="email" name="email" placeholder="Alamat Email">
                                    <?= form_error('email', '<small class="text-danger pl-3">', '</small>'); ?>
                                </div>
                            </div>
                            <!-- <div class="mb-3">
                                    <select class="form-select" name="profile_type">
                                        <option value="" selected>-- Select Profile Type --</option>
                                        <?php // foreach ($profile_type as $key) : ?>
                                            <?php //if (set_value('profile_type') == $key['profile_type_value']) : ?>
                                                <option value="<?php //$key['profile_type_value'] ?>" selected><?php //$key['profile_type_name'] ?></option>
                                            <?php //else: ?>
                                                <option value="<?php //$key['profile_type_value'] ?>"><?php //$key['profile_type_name'] ?></option>
                                            <?php //endif; ?>
                                        <?php //endforeach; ?>
                                    </select>
                                    <?php //form_error('profile_type','<div class="form-text text-danger">','</div>'); ?>
                                </div> -->
                            <div class="mb-3">
                                <div class="form-group row">
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                        <input type="password" class="form-control form-control-user" id="password1" name="password1" placeholder="Password">
                                        <?= form_error('password1', '<small class="text-danger pl-3">', '</small>'); ?>
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="password" class="form-control form-control-user" id="password2" name="password2" placeholder="Konfirmasi Password">
                                    </div>
                                </div>
                            </div>    
                            <div class="mb-3">
                                <button type="submit" class="btn btn-primary btn-user btn-block">
                                    Daftar
                                </button>
                            </div>
                        </form>
                        <hr>
                        <!-- <div class="text-center">
                            <a class="small" href="forgot-password.html">Forgot Password?</a>
                        </div> -->
                        <div class="text-center">
                            <a class="small" href="<?= base_url()?>Auth">Sudah Punya Akun? Login!</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>