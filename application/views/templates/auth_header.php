<!DOCTYPE html>
<!--
Author: Keenthemes
Product Name: Metronic - Bootstrap 5 HTML, VueJS, React, Angular & Laravel Admin Dashboard Theme
Purchase: https://1.envato.market/EA4JP
Website: http://www.keenthemes.com
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
License: For each use you must have a valid license purchased only from above link in order to legally use the theme for your project.
-->
<html lang="en">
	<!--begin::Head-->
	<head>
		<!-- <base href="../../../"> -->
		<title><?= $title; ?></title>
		<meta charset="utf-8" />
		<link rel="shortcut icon" href="<?= base_url('assets/') ?>img/Logo.png"/>
		<!--begin::Fonts-->
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
		<!--end::Fonts-->
		<!--begin::Global Stylesheets Bundle(used by all pages)-->
		<!--end::Global Stylesheets Bundle-->
		<!-- swalfire -->
		<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
		<!-- Font Awesome -->
		<!-- <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css"> -->
		<!-- Ionicons -->
		<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
		<!-- Theme style -->
		<link rel="stylesheet" href="<?= base_url("assets/"); ?>dist/css/adminlte.min.css">
		<!-- <link rel="stylesheet" href="dist/css/adminlte.min.css"> -->

		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
		<link rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/dataTables.bootstrap4.min.css">	

		<!-- jQuery -->
		<!-- <script src="plugins/jquery/jquery.min.js"></script> -->
		<!-- jQuery UI 1.11.4 -->
		<!-- <script src="plugins/jquery-ui/jquery-ui.min.js"></script> -->
		<!-- Bootstrap 4 -->
		<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
		<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>

		
		<!-- ChartJS -->
		<!-- <script src="plugins/chart.js/Chart.min.js"></script> -->
		<!-- AdminLTE App -->
		<!-- <script src="dist/js/adminlte.js"></script> -->
		<!-- AdminLTE for demo purposes -->
		<!-- <script src="dist/js/demo.js"></script> -->

		<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
		<script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
		<script src="https://cdn.datatables.net/1.10.25/js/dataTables.bootstrap4.min.js"></script>	

		<script src="https://cdn.jsdelivr.net/npm/bs-custom-file-input/dist/bs-custom-file-input.js"></script>	
		<script src="https://cdn.jsdelivr.net/npm/bs-custom-file-input/dist/bs-custom-file-input.min.js"></script> 

		<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
		<!-- <script src="dist/js/pages/dashboard.js"></script> -->

	</head>
	<!--end::Head-->
	<!--begin::Body-->
	<!-- <body id="kt_body" class="bg-body"> -->
