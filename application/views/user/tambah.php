<?= flashdata("msg");set_flashdata("msg", "") ?>
<div class="" id="">
    <div class="" id="">
        <div id="" class="container-fluid">
            <div class="row g-5 g-xl-8 mx-auto">
                <div class="col-xl-12">
                    <div class="card bg-body-white card-xl-stretch mb-xl-8">
                        <div class="card-header border-0 pt-5">
                            <div class="row" style="min-width: 100%;">
                                <div class="col-6">
                                    <h3 class=" card-title align-items-start flex-column">
                                        <span class="card-label d-inline fw-bolder text-dark">Tambah Data User</span>
                                        <!-- <span class="text-muted mt-1 fw-bold fs-7">Data user</span> -->
                                    </h3>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <form action="" method="POST">
                                <div class="mb-3">
                                    <label for="nama" class="form-label">Nama</label>
                                    <input type="text" class="form-control" id="nama" name="nama" placeholder="Masukan Nama" value="<?= set_value('nama'); ?>">
                                    <?= form_error('nama','<div class="form-text text-danger">','</div>'); ?>
                                </div>
                                <div class="mb-3">
                                    <label for="email" class="form-label">Email</label>
                                    <input type="text" class="form-control" id="email" name="email" placeholder="Masukan Email" value="<?= set_value('email'); ?>">
                                    <?= form_error('email','<div class="form-text text-danger">','</div>'); ?>
                                </div>
                                <div class="mb-3">
                                    <label for="password" class="form-label">Password</label>
                                    <input type="password" class="form-control" id="password" name="password" placeholder="Masukan Password" value="<?= set_value('password'); ?>">
                                    <?= form_error('password','<div class="form-text text-danger">','</div>'); ?>
                                </div>
                                <div class="mb-3">
                                    <label for="id_role" class="form-label">Role</label>
                                    <select class="form-control" name="id_role">
                                        <option value="" selected>-- Select Role Type --</option>
                                        <?php foreach ($role as $key) : ?>
                                            <?php if (set_value('role') == $key['id_role']) : ?>
                                                <option value="<?= $key['id_role'] ?>" selected><?= $key['nama_role'] ?></option>
                                            <?php else: ?>
                                                <option value="<?= $key['id_role'] ?>"><?= $key['nama_role'] ?></option>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                    </select>
                                    <?= form_error('id_role','<div class="form-text text-danger">','</div>'); ?>
                                </div>
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>