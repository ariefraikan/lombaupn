<?= flashdata("msg");set_flashdata("msg", "") ?>
<div class="row">
    <div class="col-md-12">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-dark h4">Tambah Data User</h6>
            </div>
            <div class="card-body">
                <form action="" method="POST">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="mb-3">
                                <div class="form-group" class="form-label">
                                    <label for="Username">Username</label>
                                    <input class="form-control" type="text" name="nama" id="nama" placeholder="Masukan Username" value="<?= $data["nama"]; ?>">
                                    <?= form_error("Username", '<small class="text-danger">', '</small>'); ?>
                                </div>
                            </div>
                            <div class="mb-3">
                                <div class="form-group" class="form-label">
                                    <label for="Password">Password Baru</label>
                                    <input class="form-control" type="hidden" name="password1" id="password1" value="<?= $data["password"]; ?>">
                                    <input class="form-control" type="password" name="password" id="password" placeholder="Masukan Password Baru" value="">
                                    <?= form_error("Password", '<small class="text-danger">', '</small>'); ?>
                                </div>
                            </div>
                            <div class="mb-3">
                                <div class="form-group">
                                    <label for="id_role" class="form-label">ID Role</label>
                                    <div class="input-group">
                                        <select class="form-control" name="id_role" id="id_role">
                                            <option selected>Pilih Role</option>
                                            <?php foreach ($role as $key) : ?>
                                                <?php if ($data["id_role"] == $key['id_role']) : ?>
                                                    <option value="<?= $key['id_role'] ?>" selected><?= $key['nama_role'] ?></option>
                                                <?php else: ?>
                                                    <option value="<?= $key['id_role'] ?>"><?= $key['nama_role'] ?></option>
                                                <?php endif; ?>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                    <?= form_error("IDRole", '<small class="text-danger">','</small>'); ?>
                                </div> 
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-dark w-100">Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>