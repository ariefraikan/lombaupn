<?= flashdata("msg");set_flashdata("msg", "") ?>
<div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <div class="row" style="min-width: 100%;">
                    <div class="col-6">
                        <h3 class="">Data User</h3>
                    </div>
                    <div class="col-6 text-right">
                        <a class="btn btn-primary btn-sm float-end" href="<?= base_url('user/tambah') ?>">
                            <i class="fas fa-plus"></i>
                            Tambah Data
                        </a>
                    </div>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <div class="table-responsive">
                    <table id="table" class="table">
                    <thead>
                    <tr>
                        <th>Nama</th>
                        <th>Email</th>
                        <th>Role</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tfoot>
                    </table>
                </div>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
            <script>
                $("#table").DataTable({
                    "responsive": true,
                    "processing": true,
                    "serverSide": true,
                    "ajax": {
                        "url": "<?= base_url("user/datatable"); ?>",
                        "type": "POST"
                        }
                })
            </script>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
</div>
    