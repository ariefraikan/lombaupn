<?= flashdata("msg");set_flashdata("msg", "") ?>

<div class="row">
    <div class="col-md-12">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-dark h4">Tambah Data User</h6>
            </div>
            <div class="card-body">
                <form action="" method="POST" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="mb-3">
                                <label for="nama_ormawa" class="form-label">Username</label>
                                <input class="form-control" type="text" name="nama_ormawa" id="nama_ormawa" placeholder="Masukan Nama Ormawa" value="<?= $data["nama_ormawa"]; ?>">
                                <?= form_error("nama_ormawa", '<small class="text-danger">', '</small>'); ?>
                            </div>
                            <div class="mb-3">
                                <label for="email" class="form-label">Email</label>
                                <?php foreach($user as $key) : ?>
                                <input class="form-control" type="text" name="email" id="email" placeholder="Masukan Email" value="<?= $key["email"]; ?>">
                                <?= form_error("email", '<small class="text-danger">', '</small>'); ?>
                                <?php endforeach; ?>
                            </div>
                            <div class="mb-3">
                                <label for="password" class="form-label">Password</label>
                                <?php foreach($user as $key) : ?>
                                <input class="form-control" type="hidden" name="password1" id="password1" value="<?= $key["password"]; ?>">
                                <input class="form-control" type="text" name="password" id="password" placeholder="Masukan Password" value="">
                                <?= form_error("password", '<small class="text-danger">', '</small>'); ?>
                                <?php endforeach; ?>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="mb-3">
                                <label for="berkas" class="form-label">Logo (.png/.jpeg)(max: 5 mb)</label>
                                <div class="custom-file">
                                    <input class="form-control" type="hidden" name="berkas1" id="berkas1" value="<?= $data["logo_ormawa"]; ?>">
                                    <input type="file" class="form-control-input" id="berkas" name="berkas" accept="image/png, image/jpeg, image/svg" value="<?= set_value('berkas'); ?>">
                                    
                                    <?= form_error('berkas','<div class="form-text text-danger">','</div>'); ?>
                                </div>
                            </div> 
                        </div>
                    </div>
                    <button type="submit" class="btn btn-dark w-100">Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>