<?= flashdata("msg");set_flashdata("msg", "") ?>

<div class="row">
    <div class="col-md-12">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-dark h4">Tambah Data Event</h6>
            </div>
            <div class="card-body">
                <form action="" method="POST" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="mb-3">
                                <label for="nama_event" class="form-label">Nama</label>
                                <input class="form-control" type="text" name="nama_event" id="nama_event" placeholder="Masukan Nama Event" value="<?= $data["nama_event"]; ?>">
                                <?= form_error("nama_event", '<small class="text-danger">', '</small>'); ?>
                            </div>
                            <div class="mb-3">
                                <label for="deskripsi_event" class="form-label">Deskripsi</label>
                                <textarea class="form-control"  name="deskripsi_event" id="deskripsi_event" placeholder="Masukan Deskripsi Event" value="<?= $data["deskripsi_event"]; ?>"><?= $data["deskripsi_event"]; ?></textarea>
                                <?= form_error("deskripsi_event", '<small class="text-danger">', '</small>'); ?>
                            </div>
                            <div class="mb-3">
                                <label for="waktu_event" class="form-label">Waktu</label>
                                <input class="form-control" type="hidden" name="waktu_event1" id="waktu_event1" value="<?= $data["waktu_event"]; ?>">
                                <input class="form-control" type="datetime-local" name="waktu_event" id="waktu_event" placeholder="Masukan Password" value="">
                                <?= form_error("waktu_event", '<small class="text-danger">', '</small>'); ?>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="mb-3">
                                <label for="id_ormawa" class="form-label">Ormawa</label>
                                    <select class="form-control" name="id_ormawa">
                                        <option value="" selected>-- Select Ormawa --</option>
                                        <?php foreach ($ormawa as $key) : ?>
                                            <?php if ($data["id_ormawa"] == $key['id_ormawa']) : ?>
                                                <option value="<?= $key['id_ormawa'] ?>" selected><?= $key['nama_ormawa'] ?></option>
                                            <?php else: ?>
                                                <option value="<?= $key['id_ormawa'] ?>"><?= $key['nama_ormawa']?></option>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                    </select>
                                    <?= form_error('id_ormawa','<div class="form-text text-danger">','</div>'); ?>
                            </div>
                            <div class="mb-3">
                                <label for="berkas" class="form-label">Gambar Event (.png/.jpeg)(max: 5 mb)</label>
                                <div class="custom-file">
                                    <input class="form-control" type="hidden" name="berkas1" id="berkas1" value="<?= $data["gambar_event"]; ?>">
                                    <input type="file" class="form-control-input" id="berkas" name="berkas" accept="image/png, image/jpeg, image/svg" value="<?= set_value('berkas'); ?>">
                                    
                                    <?= form_error('berkas','<div class="form-text text-danger">','</div>'); ?>
                                </div>
                            </div> 
                        </div>
                    </div>
                    <button type="submit" class="btn btn-dark w-100">Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>