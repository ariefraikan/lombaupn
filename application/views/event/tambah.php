<?= flashdata("msg");set_flashdata("msg", "") ?>
<div class="" id="">
    <div class="" id="">
        <div id="" class="container-fluid">
            <div class="row g-5 g-xl-8 mx-auto">
                <div class="col-xl-12">
                    <div class="card bg-body-white card-xl-stretch mb-xl-8">
                        <div class="card-header border-0 pt-5">
                            <div class="row" style="min-width: 100%;">
                                <div class="col-6">
                                    <h3 class=" card-title align-items-start flex-column">
                                        <span class="card-label d-inline fw-bolder text-dark">Tambah Data Event</span>
                                        <!-- <span class="text-muted mt-1 fw-bold fs-7">Data user</span> -->
                                    </h3>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <form action="" method="POST" enctype="multipart/form-data">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="mb-3">
                                            <label for="nama_event" class="form-label">Nama</label>
                                            <input type="text" class="form-control" id="nama_event" name="nama_event" placeholder="Masukan Nama Event" value="<?= set_value('nama_event'); ?>">
                                            <?= form_error('nama_event','<div class="form-text text-danger">','</div>'); ?>
                                        </div>
                                        <div class="mb-3">
                                            <label for="deskripsi_event" class="form-label">Deskripsi</label>
                                            <textarea class="form-control" id="deskripsi_event" name="deskripsi_event" placeholder="Masukan Deskripsi Event" value="<?= set_value('deskripsi_event'); ?>"></textarea>
                                            <?= form_error('deskripsi_event','<div class="form-text text-danger">','</div>'); ?>
                                        </div>
                                        <div class="mb-3">
                                            <label for="waktu_event" class="form-label">Waktu</label>
                                            <input type="datetime-local" class="form-control" id="waktu_event" name="waktu_event" placeholder="" value="<?= set_value('waktu_event'); ?>">
                                            <?= form_error('waktu_event','<div class="form-text text-danger">','</div>'); ?>
                                        </div>
                                    </div>
                                    <div class="col-md-6">  
                                        <div class="mb-3">
                                            <label for="id_ormawa" class="form-label">Ormawa</label>
                                            <select class="form-control" name="id_ormawa">
                                                <option value="" selected>-- Select Ormawa --</option>
                                                <?php foreach ($ormawa as $key) : ?>
                                                    <?php if (set_value('ormawa') == $key['id_ormawa']) : ?>
                                                        <option value="<?= $key['id_ormawa'] ?>" selected><?= $key['nama_ormawa'] ?></option>
                                                    <?php else: ?>
                                                        <option value="<?= $key['id_ormawa'] ?>"><?= $key['nama_ormawa'] ?></option>
                                                    <?php endif; ?>
                                                <?php endforeach; ?>
                                            </select>
                                            <?= form_error('id_ormawa','<div class="form-text text-danger">','</div>'); ?>
                                        </div>
                                        <div class="form-group mb-3">
                                            <label for="berkas" class="form-label">Gambar Event (.png/.jpeg)(max: 5 mb)</label>
                                            <div class="custom-file">
                                                <input type="file" class="form-control-input" id="berkas" name="berkas" accept="image/png, image/jpeg, image/svg" value="<?= set_value('berkas'); ?>">
                                                <!-- <label class="custom-file-label" for="berkas">Choose file...</label> -->
                                                <?= form_error('berkas','<div class="form-text text-danger">','</div>'); ?>
                                            </div>
                                            <!-- <div class="form-group mb-3">
							
                                            <label for="proposal" class="form-label">Proposal (.pdf)(max: 9 MB)</label>
                                            <input type="file" name="berkas" id="berkas" class="form-control-file" accept="application/pdf" data-max-size="32154" required>
                                        </div> -->
                                        </div>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>