<!--begin::Head-->
<head><base href="../../../">
		<title>PKL-SMK</title>
		<meta charset="utf-8" />
		<meta name="description" content="PKL-SMK" />
		<meta name="keywords" content="PKL-SMK" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<meta property="og:locale" content="en_US" />
		<meta property="og:type" content="article" />
		<meta property="og:title" content="PKL-SMK" />
		<!-- <meta property="og:url" content="https://keenthemes.com/metronic" />
		<meta property="og:site_name" content="Keenthemes | Metronic" />
		<link rel="canonical" href="https://preview.keenthemes.com/metronic8" /> -->
		<link rel="shortcut icon" href="<?= base_url("assets/metronic/")?>media/logos/bandung-logo.png" />
		<!--begin::Fonts-->
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
		<!--end::Fonts-->
		<!--begin::Page Vendor Stylesheets(used by this page)-->
		<link href="<?= base_url("assets/metronic/")?>plugins/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
		<!--end::Page Vendor Stylesheets-->
		<!--begin::Global Stylesheets Bundle(used by all pages)-->
		<link href="<?= base_url("assets/metronic/")?>plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
		<link href="<?= base_url("assets/metronic/")?>css/style.bundle.css" rel="stylesheet" type="text/css" />
		<!--end::Global Stylesheets Bundle-->
	</head>
	<!--end::Head-->