-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 20, 2021 at 04:16 PM
-- Server version: 10.4.20-MariaDB
-- PHP Version: 8.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `event_upn`
--

-- --------------------------------------------------------

--
-- Table structure for table `event`
--

CREATE TABLE `event` (
  `id_event` int(11) NOT NULL,
  `id_ormawa` int(11) NOT NULL,
  `nama_event` varchar(200) NOT NULL,
  `deskripsi_event` text NOT NULL,
  `gambar_event` varchar(200) NOT NULL,
  `waktu_event` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `event`
--

INSERT INTO `event` (`id_event`, `id_ormawa`, `nama_event`, `deskripsi_event`, `gambar_event`, `waktu_event`) VALUES
(2, 4, 'event 1', 'ini event 1', '1711493372.jpeg', '2021-11-20T21:05');

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id_menu` int(11) NOT NULL,
  `namamenu` varchar(50) NOT NULL,
  `url_menu` varchar(100) NOT NULL,
  `icon_menu` varchar(100) NOT NULL,
  `is_head` int(11) NOT NULL,
  `level_menu` int(11) NOT NULL,
  `menu_head` int(11) NOT NULL DEFAULT 0,
  `sort` int(11) NOT NULL,
  `head_title` varchar(200) NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT 1
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id_menu`, `namamenu`, `url_menu`, `icon_menu`, `is_head`, `level_menu`, `menu_head`, `sort`, `head_title`, `is_active`) VALUES
(45, 'Manajemen User', '/lombaupn/user', 'fas fa-fw fa-user', 0, 1, 0, 4, 'Manajemen Akun', 1),
(42, 'Role', '/lombaupn/role', 'fas fa-fw fa-id-card', 0, 1, 0, 3, 'Pengaturan Web', 1),
(46, 'Dashboard', '/lombaupn/dapur', 'nav-icon fas fa-tachometer-alt', 0, 1, 0, 1, 'Dashboard Admin', 1),
(36, 'Menu', '/lombaupn/menu', 'fas fa-fw fa-book', 0, 1, 0, 2, 'Pengaturan Web', 1),
(48, 'Manajemen Ormawa', '/lombaupn/ormawa', 'fas fa-fw fa-university', 0, 1, 0, 5, 'Manajemen Akun', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ormawa`
--

CREATE TABLE `ormawa` (
  `id_ormawa` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `nama_ormawa` varchar(200) NOT NULL,
  `logo_ormawa` varchar(200) NOT NULL,
  `is_active` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ormawa`
--

INSERT INTO `ormawa` (`id_ormawa`, `id_user`, `nama_ormawa`, `logo_ormawa`, `is_active`) VALUES
(4, 12, 'Android', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE `role` (
  `id_role` int(11) NOT NULL,
  `nama_role` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`id_role`, `nama_role`) VALUES
(1, 'Admin'),
(2, 'Ormawa'),
(3, 'Mahasiswa');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id_user` int(11) NOT NULL,
  `nama` varchar(256) NOT NULL,
  `email` varchar(256) NOT NULL,
  `password` varchar(128) NOT NULL,
  `id_role` int(11) NOT NULL,
  `id_profile_type` tinyint(1) NOT NULL COMMENT '1=user, 2=admin',
  `is_active` tinyint(1) NOT NULL DEFAULT 1 COMMENT '1=aktif, 2=nonaktif',
  `created_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id_user`, `nama`, `email`, `password`, `id_role`, `id_profile_type`, `is_active`, `created_at`) VALUES
(2, 'Arief Juwanda', 'ariefraikan@gmail.com', '$2y$10$/92CBtP6f59WgX1fnWqobOdasxTswz2S1mgrkB01CG0EVsyTP7t0y', 1, 1, 1, 2021),
(12, 'arief juwanda', 'test@gmail.com', '$2y$10$/92CBtP6f59WgX1fnWqobOdasxTswz2S1mgrkB01CG0EVsyTP7t0y', 2, 0, 1, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `event`
--
ALTER TABLE `event`
  ADD PRIMARY KEY (`id_event`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id_menu`);

--
-- Indexes for table `ormawa`
--
ALTER TABLE `ormawa`
  ADD PRIMARY KEY (`id_ormawa`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id_role`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `event`
--
ALTER TABLE `event`
  MODIFY `id_event` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id_menu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT for table `ormawa`
--
ALTER TABLE `ormawa`
  MODIFY `id_ormawa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `role`
--
ALTER TABLE `role`
  MODIFY `id_role` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
